﻿using System.Collections.Generic;

namespace ReadonlyList
{
    //задача - реализовать добавление и замену в односвязный список "только для чтени"

    //класс для такого списка
    class ListNode<T>
    {
        public readonly T Value;
        public readonly ListNode<T> Next;
        public ListNode(T value, ListNode<T> next = null){
            this.Value = value;
            this.Next = next;
        }
    }
    //методы
    static class ListExtensions
    {
        public static ListNode<T> ReplaceAt<T>(this ListNode<T> node, int idx, T Value)
        {
            Stack<ListNode<T>> tail = new Stack<ListNode<T>>();
            tail.Push(node);

            for(int i =0; i < idx; ++i)
            {
                if(node.Next == null)
                {
                    throw new System.IndexOutOfRangeException();
                }
                tail.Push(tail.Peek().Next);
            }

            ListNode<T> replacer = new ListNode<T>(Value, tail.Pop().Next);
            while (tail.Count > 0)
            {
                ListNode<T> tmp = new ListNode<T>(tail.Pop().Value, replacer);
                replacer = tmp;
            }

            return replacer;
        }

        public static ListNode<T> Append<T>(this ListNode<T> node, ListNode<T> appendix)
        {
            Stack<ListNode<T>> tail = new Stack<ListNode<T>>();
            tail.Push(node);

            while(tail.Peek().Next != null)
            {
                tail.Push(tail.Peek().Next);
            }

            ListNode<T> replacer = new ListNode<T>(tail.Pop().Value, appendix);
            while(tail.Count > 0)
            {
                ListNode<T> tmp = new ListNode<T>(tail.Pop().Value, replacer);
                replacer = tmp;
            }

            return replacer;
        }
    }
}

﻿
namespace GMapTask
{
    static class Columns
    {
        public const string Id = "Veh_Id";
        public const string Name = "Veh_Name";
        public const string Latitude = "Veh_Position_Latitude";
        public const string Longitude = "Veh_Position_Longitude";
    }
}

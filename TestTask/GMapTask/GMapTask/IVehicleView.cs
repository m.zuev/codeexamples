﻿using GMap.NET;

namespace GMapTask
{
    interface IVehicleView
    {
        int Id { get; }
        string Name { get; }
        PointLatLng Position { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace GMapTask
{
    internal class DbProxy
    {
        private const string LoadRequest = "SELECT * FROM ";

        private readonly SqlConnection servConn;
        private readonly SqlDataAdapter adapter;
        private readonly SqlCommandBuilder commandBuilder;

        private readonly DataSet dataSet;

        public IDictionary<int, IVehicleView> Load(string tableName)
        {
            IDictionary<int, IVehicleView> data = new Dictionary<int, IVehicleView>();
            try
            {
                servConn.Open();
                adapter.Fill(dataSet);

                DataTable table = dataSet.Tables[0];

                if (table.Rows.Count == 0)
                {
                    throw new System.NotSupportedException("missing data");
                }

                for (int i = 0; i < table.Rows.Count; ++i)
                {
                    IVehicleView view = new VehicleView(table.Rows[i]);
                    data.Add(i, view);
                }

                dataSet.Clear();
                servConn.Close();
            }
            catch (SqlException e)
            {
                MessageBox.Show("Sql Exception:" + e.Message);
            }
            return data;
        }

        public void Update(IVehicleView view)
        {
            servConn.Open();
            adapter.Fill(dataSet);

            DataTable table = dataSet.Tables[0];

            if (table.Rows.Count == 0)
            {
                throw new System.NotSupportedException("missing data");
            }

            table.Rows[view.Id][Columns.Latitude] = view.Position.Lat;
            table.Rows[view.Id][Columns.Longitude] = view.Position.Lng;
            adapter.Update(table);

            dataSet.AcceptChanges();
            dataSet.Clear();

            servConn.Close();
        }

        public DbProxy()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["VehicleDb"].ConnectionString;
            string viewName = ConfigurationManager.AppSettings["VehicleViewName"];

            servConn = new SqlConnection(connectionString);
            adapter = new SqlDataAdapter(LoadRequest + viewName, servConn);
            commandBuilder = new SqlCommandBuilder(adapter);
            dataSet = new DataSet();
            servConn.Close();
        }
    }
}

﻿using GMap.NET.WindowsForms;
using System.Collections.Generic;

namespace GMapTask
{
    class MapMarkersView : GMapOverlay
    {
        private VehiclePresenter presenter;

        public MapMarkersView()
        {
            this.presenter = new VehiclePresenter(this);
        }

        public void Init()
        {
            this.Control.MouseDown += presenter.OnMouseDown;
            this.Control.MouseMove += presenter.OnMouseMove;
            this.Control.MouseUp += presenter.OnMouseUp;

            CreateMarkers(presenter.Load());
        }

        private void CreateMarkers(IDictionary<int,IVehicleView> viewCache)
        {
            foreach (IVehicleView view in viewCache.Values)
            {
                GMapMarker marker = new VehicleMarker(view);
                marker.ToolTipMode = MarkerTooltipMode.OnMouseOver;

                this.Markers.Add(marker);
            }
        }
    }
}

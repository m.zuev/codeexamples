﻿using System;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;

namespace GMapTask
{
    public partial class MainWindow : Form
    {
        VehiclePresenter presenter;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void gMapControl_Load(object sender, EventArgs e)
        {

        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            gMapControl.Bearing = 0;
            gMapControl.MaxZoom = 18;
            gMapControl.MinZoom = 2;
            gMapControl.Zoom = 5;
            gMapControl.MouseWheelZoomType = MouseWheelZoomType.MousePositionAndCenter;
            gMapControl.Dock = DockStyle.Fill;
            gMapControl.MapProvider = GMap.NET.MapProviders.GMapProviders.YandexMap;
            GMaps.Instance.Mode = AccessMode.ServerOnly;
            gMapControl.PolygonsEnabled = true;

            MapMarkersView overlay = new MapMarkersView();
            gMapControl.Overlays.Add(overlay);
            overlay.Init();
        }
    }
}

﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMapTask.DragNDrop;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;

namespace GMapTask
{
    internal class VehiclePresenter
    {
        private readonly DbProxy dataProxy = new DbProxy();
        private readonly IDragAndDropContext dragAndDrop = new DragAndDropContext();

        private readonly GMapOverlay mapOverlay;

        public GMapMarker Target { get; set; }

        public VehiclePresenter(GMapOverlay overlay)
        {
            this.mapOverlay = overlay;
        }

        public void OnMouseDown(object sender, MouseEventArgs args)
        {
            if (mapOverlay.Markers.Count <= 0)
            {
                return;
            }

            for(int i =0; i < mapOverlay.Markers.Count; ++i)
            {
                if (mapOverlay.Markers[i].IsMouseOver)
                {
                    OnMarkerPress(mapOverlay.Markers[i], args);
                    return;
                }
            }
        }

        public void OnMouseUp(object sender, MouseEventArgs args)
        {
            if (dragAndDrop.IsDragActive())
            {
                PointLatLng mapPos = mapOverlay.Control.FromLocalToLatLng(args.Location.X, args.Location.Y);
                IVehicleView view = dragAndDrop.Selection.View;
                dragAndDrop.OnMouseUp(mapPos);
                dataProxy.Update(view);
            }
        }

        public void OnMouseMove(object sender, MouseEventArgs args)
        {
            if (dragAndDrop.IsDragActive())
            {
                PointLatLng mapPos = mapOverlay.Control.FromLocalToLatLng(args.Location.X, args.Location.Y);
                dragAndDrop.OnMouseMove(mapPos);
            }
        }

        public IDictionary<int, IVehicleView> Load()
        {
            string viewName = ConfigurationManager.AppSettings["VehicleViewName"];
            return dataProxy.Load(viewName);
        }

        private void OnMarkerPress(GMapMarker mark, MouseEventArgs mouseArgs)
        {
            if (dragAndDrop.IsDragActive()) { return; }

            VehicleMarker vehicle = mark as VehicleMarker;

            if (vehicle == null) { return; }

            dragAndDrop.OnMouseDown(vehicle);
        }

    }
}

﻿using GMap.NET.WindowsForms.Markers;

namespace GMapTask
{
    class VehicleMarker : GMarkerGoogle
    {
        private const GMarkerGoogleType MarkerType = GMarkerGoogleType.blue;

        public readonly IVehicleView View;
        
        public VehicleMarker(IVehicleView view) : base(view.Position, MarkerType)
        {
            this.View = view;
            this.ToolTipText = view.Name;
        }
    }
}

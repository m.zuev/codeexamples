﻿using GMap.NET;

namespace GMapTask.DragNDrop
{
    abstract class BasicState : IDragAndDropState
    {
        protected IDragAndDropContext context;

        public BasicState(IDragAndDropContext context)
        {
            this.context = context;
        }
        
        public abstract VehicleMarker Selection { get; set; }

        public abstract void OnPointerDown(VehicleMarker marker);
        public abstract void OnPointerMove(PointLatLng position);
        public abstract void OnPointerUp(PointLatLng position);
    }
}

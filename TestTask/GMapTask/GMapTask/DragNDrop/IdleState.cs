﻿
using GMap.NET;

namespace GMapTask.DragNDrop
{
    class IdleState : BasicState
    {
        public IdleState(IDragAndDropContext context) : base(context) { }

        public override VehicleMarker Selection { get => null; set => throw new System.NotSupportedException(); }

        public override void OnPointerDown(VehicleMarker marker)
        {
            IDragAndDropState newState = context.GetDragState;
            newState.Selection = marker;
            context.SetState(newState);
        }

        public override void OnPointerMove(PointLatLng position)
        {
        }

        public override void OnPointerUp(PointLatLng position)
        {
        }
    }
}

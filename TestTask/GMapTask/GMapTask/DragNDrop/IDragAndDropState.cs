﻿using GMap.NET;

namespace GMapTask.DragNDrop
{
    internal interface IDragAndDropState
    {
        VehicleMarker Selection { get; set; }
        void OnPointerDown(VehicleMarker marker);
        void OnPointerMove(PointLatLng position);
        void OnPointerUp(PointLatLng position);
    }
}

﻿using GMap.NET;

namespace GMapTask.DragNDrop
{
    class DragAndDropContext : IDragAndDropContext
    {
        private IDragAndDropState currentState;

        private IDragAndDropState idleState;
        private IDragAndDropState dragState;

        public IDragAndDropState GetDragState => dragState;
        public IDragAndDropState GetIdleState => idleState;

        public VehicleMarker Selection => currentState.Selection;

        public DragAndDropContext()
        {
            idleState = new IdleState(this);
            dragState = new DragState(this);
            currentState = idleState;
        }

        public bool IsDragActive()
        {
            return currentState == dragState;
        }

        public void SetState(IDragAndDropState s)
        {
            currentState = s;
        }

        public void OnMouseDown(VehicleMarker marker)
        {
            currentState.OnPointerDown(marker);
        }

        public void OnMouseMove(PointLatLng position)
        {
            currentState.OnPointerMove(position);
        }

        public void OnMouseUp(PointLatLng position)
        {
            currentState.OnPointerUp(position);
        }
    }
}

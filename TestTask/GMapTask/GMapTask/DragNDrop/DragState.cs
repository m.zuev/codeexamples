﻿using GMap.NET;

namespace GMapTask.DragNDrop
{
    internal class DragState : BasicState
    {
        private VehicleMarker selectedMarker;

        public DragState(IDragAndDropContext context) : base(context) { }

        public override VehicleMarker Selection {
            get => selectedMarker;
            set
            {
                selectedMarker = value;
            }
        }

        public override void OnPointerDown(VehicleMarker marker) { }

        public override void OnPointerMove(PointLatLng position)
        {
            selectedMarker.Position = position;
        }

        public override void OnPointerUp(PointLatLng position)
        {
            context.SetState(context.GetIdleState);
            selectedMarker.Position = position;
            selectedMarker.View.Position = position;
            selectedMarker = null;
        }
    }
}

﻿using GMap.NET;

namespace GMapTask.DragNDrop
{
    interface IDragAndDropContext
    {
        IDragAndDropState GetDragState { get; }
        IDragAndDropState GetIdleState { get; }

        VehicleMarker Selection { get; }

        void OnMouseDown(VehicleMarker marker);
        void OnMouseMove(PointLatLng position);
        void OnMouseUp(PointLatLng position);

        bool IsDragActive();
        void SetState(IDragAndDropState s);
    }
}

﻿using GMap.NET;
using System.Data;

namespace GMapTask
{
    class VehicleView : IVehicleView
    {

        public int Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public PointLatLng Position
        {
            get;
            set;
        }

        public VehicleView(DataRow dataRow)
        {
            this.Id = (int)dataRow[Columns.Id];
            this.Name = dataRow[Columns.Name].ToString();

            this.Position = new PointLatLng(
                float.Parse(dataRow[Columns.Latitude].ToString()),
                float.Parse(dataRow[Columns.Longitude].ToString())
            );
        }

    }
}

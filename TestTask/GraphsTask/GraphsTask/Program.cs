﻿
using System.Collections.Generic;

namespace GraphsTask
{
    //задача на обход графа и поиск в нем
    public class TreeNode<TValue>
    {
        public TValue Value;
        public TreeNode<TValue>[] Leafs;
        public delegate bool Predicate(TValue value);
        //обход в ширину
        public static int VisitAllNodesBFT(TreeNode<TValue> basicNode)
        {
            Queue<TreeNode<TValue>> discovered = new Queue<TreeNode<TValue>>();
            HashSet<TreeNode<TValue>> visited = new HashSet<TreeNode<TValue>>();
            discovered.Enqueue(basicNode);

            while (discovered.Count > 0)
            {
                TreeNode<TValue> processed = discovered.Dequeue();
                if (visited.Add(processed))
                    for (int i = 0; i < processed.Leafs.Length; ++i)
                {
                    discovered.Enqueue(processed.Leafs[i]);
                }
            }
            return visited.Count;
        }
        //обход в глубину
        public static int VisitAllNodesDFT(TreeNode<TValue> basicNode)
        {
            Stack<TreeNode<TValue>> discovered = new Stack<TreeNode<TValue>>();
            HashSet<TreeNode<TValue>> visited = new HashSet<TreeNode<TValue>>();
            discovered.Push(basicNode);

            while (discovered.Count > 0)
            {
                TreeNode<TValue> processed = discovered.Pop();
                if (visited.Add(processed))
                {
                    for (int i = 0; i < processed.Leafs.Length; ++i)
                    {
                        discovered.Push(processed.Leafs[i]);
                    }
                }
            }
            return visited.Count;
        }

        //бонус - поиск в дереве
        //поиск в ширину
        public static TreeNode<TValue> BFS(TreeNode<TValue> basicNode, Predicate predicate)
        {
            //используем очередь чтобы не страдать рекурсией
            Queue<TreeNode<TValue>> discovered = new Queue<TreeNode<TValue>>();
            HashSet<TreeNode<TValue>> visited = new HashSet<TreeNode<TValue>>();
            discovered.Enqueue(basicNode);

            while (discovered.Count > 0)
            {
                TreeNode<TValue> processed = discovered.Dequeue();
                if (visited.Add(processed))
                {
                    for (int i = 0; i < processed.Leafs.Length; ++i)
                    {
                        discovered.Enqueue(processed.Leafs[i]);
                    }

                    if (predicate(processed.Value))
                    {
                        return processed;
                    }
                }
            }
            return null;
        }
        //поиск в глубину
        public static TreeNode<TValue> DFS(TreeNode<TValue> basicNode, Predicate predicate)
        {
            //используем стек чтобы не страдать рекурсией
            Stack<TreeNode<TValue>> discovered = new Stack<TreeNode<TValue>>();
            HashSet<TreeNode<TValue>> visited = new HashSet<TreeNode<TValue>>();
            discovered.Push(basicNode);

            while (discovered.Count > 0)
            {
                TreeNode<TValue> processed = discovered.Pop();
                if (visited.Add(processed))
                {
                    for (int i = 0; i < processed.Leafs.Length; ++i)
                    {
                        discovered.Push(processed.Leafs[i]);
                    }
                    if (predicate(processed.Value))
                    {
                        return processed;
                    }
                }
            }
            return null;
        }
    }

    class Program
    {
        private static TreeNode<string> CreateTestNode()
        {
            var basic = new TreeNode<string>();
            basic.Value = "var";
            return basic;
        }
        private static TreeNode<string> GetTestTree()
        {
            var basic = CreateTestNode();

            var leaf11 = CreateTestNode();
            var leaf12 = CreateTestNode();

            var leaf2 = CreateTestNode();
            var leaf21 = CreateTestNode();

            basic.Leafs = new TreeNode<string>[] { leaf11, leaf12 };
            leaf11.Leafs = new TreeNode<string>[] { leaf2 };
            leaf12.Leafs = new TreeNode<string>[] { leaf2, leaf21 };
            leaf2.Leafs = new TreeNode<string>[0];
            leaf21.Leafs = new TreeNode<string>[0];
            return basic;
        }

        static void Main(string[] args)
        {
            var tree = GetTestTree();
            int nodes = TreeNode<string>.VisitAllNodesBFT(tree);
            int nodes2 = TreeNode<string>.VisitAllNodesDFT(tree);
        }
    }
}

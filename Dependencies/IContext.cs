﻿using System;
namespace Core.Dependencies
{
    //service provider interface
    public interface IContext : ISeed
    {
        TSeed GetSeed<TSeed>() where TSeed : class, ISeed;
        ISeed GetSeed(Type type);
    }
}
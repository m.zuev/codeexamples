﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Core.Dependencies.AdvancedReflection;

namespace Core.Dependencies
{
    public delegate void ResolveAction();
    public abstract class BasicContext : MonoBehaviour, IContext, ISeed
    {
        private const BindingFlags InjectedFieldsSearchFlags = BindingFlags.NonPublic | BindingFlags.Instance;

        internal static readonly List<Type> ignoredTypes = new List<Type> { TypeOf<ISeed>.Raw, TypeOf<IInitOnResolve>.Raw, TypeOf<MonoBehaviour>.Raw };

        protected static IContext globalContext;

        protected readonly Dictionary<Type, List<KeyValuePair<FieldInfo, ISeed>>> injectionRequests = new Dictionary<Type, List<KeyValuePair<FieldInfo, ISeed>>>();
        protected readonly Dictionary<Type, Injector> injectors = new Dictionary<Type, Injector>();
        protected readonly Dictionary<IInitOnResolve, List<Type>> unresolved = new Dictionary<IInitOnResolve, List<Type>>();

        [SerializeField] protected Canvas sceneCanvas;
        [SerializeField] protected bool isTest = false;

        public TSeed GetSeed<TSeed>() where TSeed : class, ISeed
        {
            if (injectors.ContainsKey(TypeOf<TSeed>.Raw))
            {
                return injectors[typeof(TSeed)].Implementation as TSeed;
            }
            return null;
        }

        public ISeed GetSeed(Type type)
        {
            if (injectors.ContainsKey(type))
            {
                return injectors[type].Implementation as ISeed;
            }
            return null;
        }

        protected virtual void Resolve(Type seedType)
        {
            if (!injectors.Keys.Contains(seedType))
            {
                throw new ArgumentException("No Such seed {0}", seedType.Name);
            }
            for (IEnumerator<KeyValuePair<FieldInfo, ISeed>> requests = injectionRequests[seedType].GetEnumerator(); requests.MoveNext();)
            {
                requests.Current.Key.SetValue(requests.Current.Value, injectors[seedType].Implementation);
                IInitOnResolve candidate = requests.Current.Value as IInitOnResolve;
                if (candidate != null)
                {
                    unresolved[candidate].Remove(requests.Current.Key.FieldType);
                }
            }
            injectionRequests.Remove(seedType);
        }

        protected virtual void Start()
        {
            IEnumerable<ISeed> seeds = GetComponentsInChildren<ISeed>(true).
                Concat(sceneCanvas.GetComponentsInChildren<ISeed>(true));

            GenerateInjectors(seeds);
            GenerateRequests(seeds);
            List<Type> resolveList = injectionRequests.Keys.ToList();
            foreach (Type seedType in resolveList)
            {
                Resolve(seedType);
            }
            IEnumerable<IInitOnResolve> toResolve = seeds.Select(s => s as IInitOnResolve).Where(o => o != null);
            foreach (IInitOnResolve unres in toResolve)
            {
                ProcessIntitalization(unres);
            }
        }

        private void GenerateInjectors(IEnumerable<ISeed> seeds)
        {
            foreach (ISeed seed in seeds)
            {
                Injector injector = new Injector(seed);
                foreach (Type @interface in injector.ImplementedSeeds)
                {
                    injectors[@interface] = injector;
                }
            }
        }

        private void GenerateRequests(IEnumerable<ISeed> seeds)
        {
            foreach (ISeed seed in seeds)
            {
                Type seedType = seed.GetType();
                TypeInfo typeInfo = seed.GetType().GetTypeInfo();
                IEnumerable<FieldInfo> injected = typeInfo.GetFields(InjectedFieldsSearchFlags).Where(fi => fi.GetCustomAttribute<InjectionAttribute>() != null);
                foreach (FieldInfo fi in injected)
                {
                    Type fieldType = fi.FieldType;
                    if (injectionRequests.ContainsKey(fieldType))
                    {
                        injectionRequests[fieldType].Add(new KeyValuePair<FieldInfo, ISeed>(fi, seed));
                    }
                    else
                    {
                        injectionRequests[fieldType] = new List<KeyValuePair<FieldInfo, ISeed>> {
                            new KeyValuePair<FieldInfo, ISeed>(fi, seed)
                        };
                    }
                }
                IInitOnResolve onResolveCandidate = seed as IInitOnResolve;
                if (onResolveCandidate != null)
                {
                    unresolved[onResolveCandidate] = injected.Select(fi => fi.FieldType).ToList();
                }
            }
        }

        private void ProcessIntitalization(IInitOnResolve candidate)
        {
            if (unresolved.ContainsKey(candidate))
            {
                if (unresolved[candidate].Count == 0)
                {
                    unresolved.Remove(candidate);
                    candidate.OnResolve();
                }
            }
        }

        protected class Injector
        {
            public readonly object Implementation;
            public Type[] ImplementedSeeds;
            public Injector(ISeed impl)
            {
                Implementation = impl;
                ImplementedSeeds = impl.GetType().
                    GetTypeInfo().
                    ImplementedInterfaces.
                    Where(t => !BasicContext.ignoredTypes.Contains(t)).
                    Where(type => typeof(ISeed).IsAssignableFrom(impl.GetType())).ToArray();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace Core.Dependencies
{
    //service provider inside scene to provide reqired services in scene from global context or from inside the scene
    public class SceneContext : BasicContext, IContext
    {
        protected override void Resolve(Type seedType)
        {
            if (!injectors.Keys.Contains(seedType))
            {
                if (isTest)
                {
                    injectors[seedType] = new Injector(TestUtility.MockExtensions.GetSeed(seedType));
                }
                if(globalContext == null)
                { throw new ArgumentException("No Such seed", seedType.Name); }

                ISeed seed = globalContext.GetSeed(seedType) as ISeed;
                if (globalContext.GetSeed(seedType) == null)
                { throw new ArgumentException("No Such seed", seedType.Name); }
                else
                {
                    injectors[seedType] = new Injector(seed);
                }
            }

            for (IEnumerator<KeyValuePair<FieldInfo, ISeed>> requests = injectionRequests[seedType].GetEnumerator(); requests.MoveNext();)
            {
                requests.Current.Key.SetValue(requests.Current.Value, injectors[seedType].Implementation);
                IInitOnResolve candidate = requests.Current.Value as IInitOnResolve;
                if (candidate != null)
                {
                    unresolved[candidate].Remove(requests.Current.Key.FieldType);
                }
            }

            injectionRequests.Remove(seedType);
        }
    }
}
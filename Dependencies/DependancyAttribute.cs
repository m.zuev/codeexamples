﻿using System;
namespace Core.Dependencies
{
    public class DependancyAttribute
    {
        private readonly Type[] types;

        DependancyAttribute(params Type[] types)
        {
            this.types = types;
        }
    }
}
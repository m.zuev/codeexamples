﻿namespace Core.Dependencies
{
    public interface IInitOnResolve
    {
        void OnResolve();
    }
}

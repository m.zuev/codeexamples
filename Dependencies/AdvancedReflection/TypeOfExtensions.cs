﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Dependencies.AdvancedReflection { 
    public static class TypeOf<T> 
    {
        public static readonly Type Raw = typeof(T);
        public static readonly string Name = Raw.Name;
        public static readonly Assembly Assembly = Raw.Assembly;
        public static readonly bool IsValueType = Raw.IsValueType;
        public static readonly List<Attribute> attributes = Raw.GetCustomAttributes().ToList();
        //public static readonly List<Dependencies>
    }

}

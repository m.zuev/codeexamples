﻿using System;

namespace Core.Dependencies.GlobalContext
{
    internal interface IGlobalContext : IContext
    {
    }
    //background scene service provider containing singletone services available to all scenes 
    public class GlobalContext : BasicContext, IGlobalContext
    {
        internal static IContext Instance
        {
            get { return (IContext) globalContext; }
        }

        protected override void Start()
        {
            if(Instance == null)
            {
                base.Start();
                if (isTest)
                {
                    return;
                    globalContext = TestUtility.MockExtensions.GetContext();
                }
                globalContext = this;
                DontDestroyOnLoad(this.gameObject);
                DontDestroyOnLoad(sceneCanvas);
                return;
            }
            if(Instance != this)
            {
                Destroy(this);
                throw new Exception("second instance of global context");
            }
        }
    }
    
}
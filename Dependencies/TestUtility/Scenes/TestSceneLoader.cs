﻿using UnityEngine.SceneManagement;

namespace Core.TestUtility
{
    public abstract class TestSceneLoader
    {
        public abstract string TestSceneName { get; }
        public void LoadTestScene()
        {
            SceneManager.LoadSceneAsync(TestSceneName, LoadSceneMode.Single);
        }
    }
}
﻿using Core.Dependencies;
using NSubstitute;
using System.Collections.Generic;
using System;

namespace Core.TestUtility
{
    public static class MockExtensions
    {
        private static readonly TestContext testContext = new TestContext();

        public static T Mock<T>()
            where T : class, ISeed
        {
            T testSeed = Substitute.For<T>();
            testContext.AddSeed(testSeed);
            return testSeed;
        }

        public static T GetSeed<T>()
            where T : class, ISeed
        {
            return testContext.GetSeed<T>();
        }

        public static ISeed GetSeed(Type type)
        {
            return testContext.GetSeed(type);
        }

        public static IContext GetContext()
        {
            return testContext;
        }
    }

    internal class TestContext : IContext
    {
        public readonly Dictionary<Type, ISeed> context = new Dictionary<Type, ISeed>();

        public ISeed GetSeed(Type type)
        {
            ISeed result = null;
            context.TryGetValue(type, out result);
            return result;
        }

        public TSeed GetSeed<TSeed>() where TSeed : class, ISeed
        {
            ISeed result = null;
            context.TryGetValue(typeof(TSeed), out result);
            return result as TSeed;
        }

        public void AddSeed<TSeed>(TSeed seed) where TSeed : class, ISeed
        {
            if (context.ContainsKey(typeof(TSeed)))
            {
                throw new Exception("Seed already mocked");
            }
            
        }
    }
}
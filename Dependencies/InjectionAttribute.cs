﻿using System;
namespace Core.Dependencies
{
    [AttributeUsage(AttributeTargets.Field)]
    public class InjectionAttribute : Attribute
    {
    }
}
